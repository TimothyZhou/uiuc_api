.. uiuc-api documentation master file, created by sphinx-quickstart on Fri Jul 31 00:30:28 2020. You can adapt this file completely to your liking, but it should at least contain the root `toctree` directive.

uiuc-api
====================================
``uiuc-api`` provides a simple, high-level interface for working with the University of Illinois at Urbana Champaign's REST API's catalog module. Note that it is still under development and may be slightly buggy.

API Reference
===============
.. module:: uiuc_api
.. autoclass:: Course
    :members:

    .. automethod:: __init__

.. autofunction:: get_course
.. admonition:: Prerequsities, Corequsities, and Constraints

    Prerequsites (``Course.prereqs``) are reprsented as a list of frozen sets, where each frozen set contains one or more options, one of which must be satisifed. For instance: ``[{CS 125}, {CS 173, MATH 213}]`` means "CS 125 and one of CS 173, MATH 213." Corequsities  (``Course.coreqs``) are represented the same way. Additional data that cannot be parsed into these forms is put into ``Course.constraints`` as a list of strings. Please note that there may not be 100% accuracy in parsing course data, as course descriptions are highly varried, and it is difficult to write an all-inclusive grammar.

.. admonition:: Credit Hours

    Credit hours are parsed by taking the *first* integer from the text of the creditHours XML attribute. This has the consequence that for courses that are "X or Y" hours, ``Course.credit_hours`` is only X. If you want to get the raw credit hour data use ``Course.raw.find("creditHours")``.

.. autofunction:: get_courses

.. admonition:: How to Use ``get_courses``

    Each call to ``get_course`` is slow as ``get_course`` calls ``fetch`` which has to query the XML data. As with most IO bound tasks, this can be streamlined via multithreading. As ``get_courses`` takes in an iterable of course names, not a single course name, and makes use of concurrency, it is more performant than multiple synchronous calls to ``get_course`` if ``max_workers`` is set to an appropriate amount. More workers translates to more initial overhead but will generally result in more courses fetched / second. If the number of courses is high, it is reccomended to set this signifigantly higher than the default, though this is heavily dependent on hardware. If you want to do something like get the entire course catalog, you may want to benchmark and figure out the ideal value for ``max_workers`` first.

.. autofunction:: subject_iterator
.. autofunction:: course_iterator
.. admonition::Catalog Iterators::

    ``subject_names`` and ``course_names`` are equivalent to calling ``subject_iterator`` and ``course_iterator`` with their defaults. Because these are lazy (yaay!) they won't cause overhead unless iterated over (getting all the course names takes one request per subject).

.. autofunction:: fetch
.. admonition:: When to Use ``fetch``

    ``fetch`` is a lower level function that directly gets XML data. For the most part, you should be using ``get_course`` or ``get_courses`` instead. You can also access the ``lxml.etree.Element`` object directly via ``Course.raw``. The output of ``fetch`` is dependent on what default arguments are passed. If no subject is passed, the data for the subject catalog is returned. If a subject but no course number is specified, it gets the catalog data for that subject. If a course number is also specified, it gets the catalog entry for that course.
